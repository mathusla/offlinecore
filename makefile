CC = g++

SOURCE_DIRECTORY = src
INCLUDE_DIRECTORY = include
OBJECT_DIRECTORY = obj

CFLAGS = -I$(INCLUDE_DIRECTORY) -lrt -O3 -DLINUX -Wall -Wextra -pedantic -D__STDC_FORMAT_MACROS -D__STDC_LIMIT_MACROS
CXXFLAGS = $(CFLAGS) -std=gnu++0x

ROOTFLAGS = `root-config --cflags --ldflags --libs`

all: startstop

$(OBJECT_DIRECTORY)/%.o: $(SOURCE_DIRECTORY)/%.c
	@mkdir $(OBJECT_DIRECTORY) &>/dev/null || true
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/%.o: $(SOURCE_DIRECTORY)/%.cxx
	@mkdir $(OBJECT_DIRECTORY) &>/dev/null || true
	$(CC) -c $(CXXFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists.o: $(SOURCE_DIRECTORY)/makehists.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists2.o: $(SOURCE_DIRECTORY)/makehists2.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists3.o: $(SOURCE_DIRECTORY)/makehists3.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists4.o: $(SOURCE_DIRECTORY)/makehists4.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists5.o: $(SOURCE_DIRECTORY)/makehists5.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists6.o: $(SOURCE_DIRECTORY)/makehists6.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehists7.o: $(SOURCE_DIRECTORY)/makehists7.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/rpcplots.o: $(SOURCE_DIRECTORY)/rpcplots.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/offlinemonitoring.o: $(SOURCE_DIRECTORY)/offlinemonitoring.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

$(OBJECT_DIRECTORY)/makehistpdfs.o: $(SOURCE_DIRECTORY)/makehistpdfs.cxx
	$(CC) -c $(CXXFLAGS) $(ROOTFLAGS) -o $@ $<

readout_demo: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o readout_demo.o)
	$(CC) $(CXXFLAGS) -o $@ $^

readout_demo2: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o readout_demo2.o)
	$(CC) $(CXXFLAGS) -o $@ $^

startstop: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o MATHUSLADataFile.o readout.o startstop.o)
	$(CC) $(CXXFLAGS) -o $@ $^

status_readout_demo: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o status_readout_demo.o)
	$(CC) $(CXXFLAGS) -o $@ $^

makehists: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehists2: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists2.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehists3: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists3.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehists4: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists4.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehists5: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists5.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehists6: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists6.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehists7: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o makehists7.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

rpcplots: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o rpcplots.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

offlinemonitoring: $(addprefix $(OBJECT_DIRECTORY)/,mathusla_file_format_v3.o timestamp.o readout.o MATHUSLADataFile.o offlinemonitoring.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

makehistpdfs:  $(addprefix $(OBJECT_DIRECTORY)/,timestamp.o makehistpdfs.o)
	$(CC) $(CXXFLAGS) $(ROOTFLAGS) -o $@ $^

clean:
	@rm $(OBJECT_DIRECTORY)/*.o &>/dev/null || true
