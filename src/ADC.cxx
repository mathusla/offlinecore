#include "OfflineData/ADC.h"

ADC::ADC(int ADC_count, int channel_number, int ADC_number) {
 
    m_adccount = ADC_count;
    m_channelnumber = channel_number;
    m_adcnumber = ADC_number;
}

int ADC::getADCCount() {
    return m_adccount;
}

int ADC::getChannelNumber() {
    return m_channelnumber;
}

int ADC::getADCNumber() {
    return m_adcnumber;
}
