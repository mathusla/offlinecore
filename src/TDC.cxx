#include "OfflineData/TDC.h"

TDC::TDC(int TDC_count, int channel_number, int start_bit, int edge_bit) {

    m_tdccount = TDC_count;
    m_channelnumber = channel_number;
    m_startbit = start_bit;
    m_edgebit = edge_bit;
}

int TDC::getTDCCount() {
    return m_tdccount;
}

int TDC::getChannelNumber() {
    return m_channelnumber;
}

int TDC::getStartBit() {
    return m_startbit;
}

int TDC::getEdgeBit() {
    return m_edgebit;
}
