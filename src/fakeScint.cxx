#include <iostream>
#include "OfflineData/ScintillatorHit.h"

main(){

ScintillatorHit ScintHit(1, 2, 3, 4, 5);
  std::cout << "X Position: " << ScintHit.getX() << std::endl;
  std::cout << "Y Position: " << ScintHit.getY() << std::endl;
  std::cout << "Z Position: " << ScintHit.getZ() << std::endl;
  std::cout << "Run Number: " << ScintHit.getRunNumber() << std::endl;
  std::cout << "Time: " << ScintHit.getRunTime() << std::endl;

}
