#include <cstdio>
#include <cinttypes>
#include "msgsvc.h"
#include "mathusla_file_format_v3.h"
#include "readout.hh"
#include "readout_demo.hh"

int main(int argc, char *argv[]) {
	if (argc != 2 && argc != 3) {
		MSG_FATAL("Usage: %s FILE [EVENT_NUMBER]", argv[0])
	}

	FILE *file = fopen(argv[1], "rb");
	if (file == NULL) {
		MSG_FATAL_ERRNO("Unable to open file %s", argv[1],)
	}

	struct FileHeader file_header;
	if (read_file_header(file, &file_header) != 0) {
		fclose(file);
		MSG_FATAL("Unable to read file header")
	}
	if (PRINT_RAW) {
		print_file_header(&file_header);
	}

	uint32_t event_number;
	int select_event = 0;
	if (argc == 3) {
		select_event = 1;
		event_number = atol(argv[2]);
	}

	struct Block block;
	struct VMEEventDataReadout vme_event_data_readout;
	uint64_t n_events = 0;
	while (get_next_block(file, &block) == 0) {
		if (select_event == 0 || (block.block_header.block_type == EVENT && block.block_header.record_number == event_number)) {
			if (PRINT_RAW) {
				print_block(&block);
			}
			if (block.block_header.block_type == EVENT) {
				n_events++;
				if (n_events % UPDATE_INTERVAL == 0) {
					MSG_INFO("Processed %" PRIu64 " events", n_events)
				}
				if (fill_vme_event_data_readout(vme_event_data_readout, block) != 0) {
					MSG_ERROR("Error in reading event %" PRIu32, block.block_header.record_number)
				}
				if (PRINT_READABLE) {
					print_vme_event_data_readout(stdout, vme_event_data_readout);
				}
			}
			if (select_event) {
				delete_block(&block);
				fclose(file);
				return 0;
			}
		}
		delete_block(&block);
	}
	fclose(file);
	MSG_INFO("%" PRIu64 " events", n_events)
	return 0;
}
