#include <inttypes.h>
#include "msgsvc.h"
#include "mathusla_file_format_v3.h"

static const uint16_t MODULE_CLASS_MASK = 0x7800;
static const unsigned MODULE_CLASS_BIT_POSITION = 11;

static const uint16_t MODULE_DATA_FORMAT_MASK = 0x07c0;
static const unsigned MODULE_DATA_FORMAT_BIT_POSITION = 6;

static const uint16_t MODULE_IDENTIFIER_MASK = 0x003f;

static const uint16_t VME_SUBBLOCK_TYPE_FLAG = 0x8000;

static const uint_fast32_t MAX_BLOCK_CONTENT_LEGNTH = (1 << sizeof (struct BlockContentLength) * 8) - 1;

static const uint_fast32_t NANOSECONDS_IN_SECOND = 1000000000;

static const unsigned SUBBLOCK_CONTENT_BYTE_ALIGNMENT = 4;

int read_file_header(FILE *stream, struct FileHeader *file_header) {
	if (fread(file_header, sizeof *file_header, 1, stream) == 1) {
		return 0;
	} else {
		if (feof(stream)) {
			MSG_ERROR("Unable to read file header: End of file")
		} else {
			MSG_ERROR_ERRNO("Unable to read file header")
		}
		return 1;
	}
}

void get_vme_subblock_type(struct VMESubblockType *vme_subblock_type, const uint16_t vme_subblock_type_word) {
	vme_subblock_type->module_class = (vme_subblock_type_word & MODULE_CLASS_MASK) >> MODULE_CLASS_BIT_POSITION;
	vme_subblock_type->module_data_format = (vme_subblock_type_word & MODULE_DATA_FORMAT_MASK) >> MODULE_DATA_FORMAT_BIT_POSITION;
	vme_subblock_type->module_identifier = vme_subblock_type_word & MODULE_IDENTIFIER_MASK;
}

uint16_t get_vme_subblock_type_word(const struct VMESubblockType *vme_subblock_type) {
	return VME_SUBBLOCK_TYPE_FLAG | ((vme_subblock_type->module_class << MODULE_CLASS_BIT_POSITION) & MODULE_CLASS_MASK)
	                              | ((vme_subblock_type->module_data_format << MODULE_DATA_FORMAT_BIT_POSITION) & MODULE_DATA_FORMAT_MASK)
	                              | (vme_subblock_type->module_identifier & MODULE_IDENTIFIER_MASK);
}

uint_fast32_t get_block_content_length(const struct Block *block) {
	return block->block_header.block_content_length.least_significant_8_bits + (block->block_header.block_content_length.middle_8_bits << 8) +  (block->block_header.block_content_length.most_significant_8_bits << (2 * 8));
}

int set_block_content_length(struct Block *block, const uint_fast32_t length) {
	if (length > MAX_BLOCK_CONTENT_LEGNTH) {
		MSG_ERROR("Length %" PRIuFAST32 "is greater than maximum block content length %" PRIuFAST32, length, MAX_BLOCK_CONTENT_LEGNTH)
		return 1;
	}
	block->block_header.block_content_length.least_significant_8_bits = length & UINT8_MAX;
	block->block_header.block_content_length.middle_8_bits = (length >> 8) & UINT8_MAX;
	block->block_header.block_content_length.most_significant_8_bits = (length >> (2 * 8)) & UINT8_MAX;
	return 0;
}

void get_block_timestamp(const struct Block *block, struct timespec *tp) {
	const uint64_t time = ((uint64_t) block->block_header.timestamp.most_significant_32_bits << 32) + block->block_header.timestamp.least_significant_32_bits;
	tp->tv_sec = time / NANOSECONDS_IN_SECOND;
	tp->tv_nsec = time % NANOSECONDS_IN_SECOND;
}

int set_block_timestamp(struct Block *block, const struct timespec *tp) {
	if (tp->tv_sec < 0) {
		MSG_ERROR("Timestamp seconds (%jd) is negative", tp->tv_sec)
		return 1;
	}
	if (tp->tv_nsec < 0) {
		MSG_ERROR("Timestamp nanoseconds (%ld) is negative", tp->tv_nsec)
		return 1;
	}
	if ((uintmax_t) tp->tv_nsec >= NANOSECONDS_IN_SECOND) {
		MSG_ERROR("Timestamp nanoseconds (%ld) is greater than number of nanoseconds in second (%" PRIuFAST32 ")", tp->tv_nsec, NANOSECONDS_IN_SECOND)
		return 1;
	}
	if ((uintmax_t) tp->tv_sec > (UINT64_MAX - (uint64_t) tp->tv_nsec) / NANOSECONDS_IN_SECOND) {
		MSG_ERROR("Timestamp value (%jd * %" PRIuFAST32 " + %ld) is greater than UINT64_MAX (%" PRIu64 ")", tp->tv_sec, NANOSECONDS_IN_SECOND, tp->tv_nsec, UINT64_MAX)
		return 1;
	}
	const uint64_t time = tp->tv_sec * NANOSECONDS_IN_SECOND + tp->tv_nsec;
	block->block_header.timestamp.least_significant_32_bits = time & UINT32_MAX;
	block->block_header.timestamp.most_significant_32_bits = time >> 32;
	return 0;
}

void initialize_file_header(struct FileHeader *file_header, const uint32_t file_format_version) {
	memcpy(file_header->file_magic_number, FILE_MAGIC_NUMBER, sizeof FILE_MAGIC_NUMBER);
	file_header->file_format_version = file_format_version;
}

void initialize_block(struct Block *block, const uint8_t block_type, const uint32_t record_number) {
	memcpy(block->block_header.block_magic_number, BLOCK_MAGIC_NUMBER, sizeof BLOCK_MAGIC_NUMBER);
	block->block_header.block_type = block_type;
	set_block_content_length(block, 0);
	block->block_header.record_number = record_number;
	block->block_content.subblocks = NULL;
	block->block_content.n_subblocks = 0;
}

void pad_subblock(struct Subblock *subblock) {
	memset(subblock->subblock_content + subblock->subblock_header.subblock_content_length, 0,
               (SUBBLOCK_CONTENT_BYTE_ALIGNMENT -
                subblock->subblock_header.subblock_content_length % SUBBLOCK_CONTENT_BYTE_ALIGNMENT)
               % SUBBLOCK_CONTENT_BYTE_ALIGNMENT);
}

int add_subblock(struct Block *block, const struct Subblock *subblock) {
	const uint_fast32_t block_content_length = get_block_content_length(block);
	const uint_fast32_t subblock_length = sizeof(struct SubblockHeader) + subblock->subblock_header.subblock_content_length + (SUBBLOCK_CONTENT_BYTE_ALIGNMENT - subblock->subblock_header.subblock_content_length % SUBBLOCK_CONTENT_BYTE_ALIGNMENT) % SUBBLOCK_CONTENT_BYTE_ALIGNMENT;
	if (block_content_length > UINT32_MAX - subblock_length) {
		MSG_ERROR("Unable to add subblock: block content length would be too large (%" PRIuFAST32 " + %" PRIuFAST32 " > UINT32_MAX (%" PRIu32 "))", block_content_length, subblock_length, UINT32_MAX)
		return 1;
	}
	if (block->block_content.n_subblocks == UINT32_MAX) {
		MSG_ERROR("Unable to add subblock: number of subblocks is too high (%" PRIuFAST32 " = UINT32_MAX)", block->block_content.n_subblocks)
		return 1;
	}
	if (realloc_handler((void **) &block->block_content.subblocks, (block->block_content.n_subblocks + 1) * sizeof(struct Subblock)) != 0) {
		return 1;
	}
	block->block_content.subblocks[block->block_content.n_subblocks] = *subblock;
	++block->block_content.n_subblocks;
	set_block_content_length(block, block_content_length + subblock_length);
	return 0;
}

int write_file_header(FILE *stream, const struct FileHeader *file_header) {
	if (fwrite(file_header, sizeof *file_header, 1, stream) != 1) {
		MSG_ERROR_ERRNO("Unable to write file header")
		return 1;
	}
	return 0;
}

FILE *create_mathusla_file(const char *filename) {
	FILE *file = fopen(filename, "wb");
	if (file == NULL) {
		MSG_ERROR_ERRNO("Unable to open file %s for writing", filename)
		return NULL;
	}
	struct FileHeader file_header;
	initialize_file_header(&file_header, FILE_FORMAT_VERSION);
	if (write_file_header(file, &file_header) != 0) {
		if (fclose(file) == EOF) {
			MSG_ERROR_ERRNO("Unable to close file %s", filename)
		}
		return NULL;
	}
	return file;
}

int write_subblock(FILE *stream, const struct Subblock *subblock) {
	if (fwrite(&subblock->subblock_header, sizeof subblock->subblock_header, 1, stream) != 1) {
		MSG_ERROR_ERRNO("Unable to write subblock header")
		return 1;
	}
	if (fwrite(subblock->subblock_content, (uint32_t) subblock->subblock_header.subblock_content_length, 1, stream) != 1) {
		MSG_ERROR_ERRNO("Unable to write subblock content")
		return 1;
	}
	int padding_length = (SUBBLOCK_CONTENT_BYTE_ALIGNMENT - subblock->subblock_header.subblock_content_length % SUBBLOCK_CONTENT_BYTE_ALIGNMENT) % SUBBLOCK_CONTENT_BYTE_ALIGNMENT;
	for (; padding_length > 0; --padding_length) {
		if (fputc(0, stream) == EOF) {
			MSG_ERROR_ERRNO("Unable to write subblock content padding")
		}
	}
	return 0;
}

int write_block(FILE *stream, const struct Block *block) {
	if (fwrite(&block->block_header, sizeof block->block_header, 1, stream) != 1) {
		MSG_ERROR_ERRNO("Unable to write block header")
		return 1;
	}
	for (uint32_t subblock_index = 0; subblock_index < block->block_content.n_subblocks; ++subblock_index) {
		if (write_subblock(stream, block->block_content.subblocks + subblock_index) != 0) {
			return 1;
		}
	}
	return 0;
}

void delete_subblock(struct Subblock *subblock) {
	free(subblock->subblock_content);
	subblock->subblock_header.subblock_content_length = 0;
}

void delete_block(struct Block *block) {
	for (; block->block_content.n_subblocks > 0; --block->block_content.n_subblocks) {
		delete_subblock(block->block_content.subblocks + block->block_content.n_subblocks - 1);
	}
	free(block->block_content.subblocks);
	block->block_content.n_subblocks = 0;
	set_block_content_length(block, 0);
}

int get_next_subblock(FILE *stream, struct Subblock *subblock) {
	if (fread(subblock, sizeof subblock->subblock_header, 1, stream) != 1) {
		if (feof(stream)) {
			MSG_ERROR("Unable to read subblock header: End of file")
		} else {
			MSG_ERROR_ERRNO("Unable to read subblock header")
		}
		return 1;
	}
	if (subblock->subblock_header.subblock_content_length == 0) {
		subblock->subblock_content = NULL;
		return 0;
	}
	const uint_fast32_t subblock_content_with_padding_length = (uint_fast32_t) subblock->subblock_header.subblock_content_length +
                                                                   (SUBBLOCK_CONTENT_BYTE_ALIGNMENT
                                                                   - subblock->subblock_header.subblock_content_length % SUBBLOCK_CONTENT_BYTE_ALIGNMENT)
                                                                   % SUBBLOCK_CONTENT_BYTE_ALIGNMENT;
	if (malloc_handler((void **) &subblock->subblock_content, subblock_content_with_padding_length) != 0) {
		return 1;
	}
	if (fread(subblock->subblock_content, subblock_content_with_padding_length, 1, stream) != 1) {
		if (feof(stream)) {
			MSG_ERROR("Unable to read subblock content: End of file")
		} else {
			MSG_ERROR_ERRNO("Unable to read subblock content")
		}
		delete_subblock(subblock);
		return 1;
	}
	return 0;
}

int get_next_block(FILE *stream, struct Block *block) {
	if (fread(block, sizeof block->block_header, 1, stream) != 1) {
		if (feof(stream)) {
			MSG_DEBUG_FULL("End of file")
			return EOF;
		} else {
			MSG_ERROR_ERRNO("Unable to read block header")
			return 1;
		}
	}
	block->block_content.subblocks = NULL;
	block->block_content.n_subblocks = 0;
	const uint_fast32_t block_content_length = get_block_content_length(block);
	if (block_content_length == 0) {
		return 0;
	}
	if (malloc_handler((void **) &block->block_content.subblocks, block_content_length / sizeof(struct SubblockHeader) * sizeof(struct Subblock)) != 0) {
		return 1;
	}
	const long start_pos = ftell(stream);
	while ((uintmax_t) ftell(stream) - start_pos < block_content_length) {
		if (get_next_subblock(stream, block->block_content.subblocks + block->block_content.n_subblocks) != 0) {
			MSG_ERROR("Unable to get subblock %" PRIuFAST32, block->block_content.n_subblocks)
			delete_block(block);
			return 1;
		}
		++block->block_content.n_subblocks;
	}
	if (realloc_handler((void **) &block->block_content.subblocks, block->block_content.n_subblocks * sizeof(struct Subblock)) != 0) {
		delete_block(block);
		return 1;
	}
	return 0;
}

void print_file_header(struct FileHeader *file_header) {
	char file_magic_number[sizeof file_header->file_magic_number + 1];
	snprintf(file_magic_number, sizeof file_header->file_magic_number + 1, "%s", file_header->file_magic_number);
	printf("file_header.file_magic_number: %s\n", file_magic_number);
	printf("file_header.file_format_version: %" PRIu32 "\n", file_header->file_format_version);
}

void print_subblock(struct Subblock *subblock) {
	printf("\tsubblock.subblock_header.subblock_type: 0x%" PRIx16 "\n", subblock->subblock_header.subblock_type);
	if (subblock->subblock_header.subblock_type & VME_SUBBLOCK_TYPE_FLAG) {
		struct VMESubblockType vme_subblock_type;
		get_vme_subblock_type(&vme_subblock_type, subblock->subblock_header.subblock_type);
		printf("\tmodule_class: 0x%x (", vme_subblock_type.module_class);
		switch (vme_subblock_type.module_class) {
			case CONTROLLER:
				printf("controller");
				break;
			case TDC:
				printf("TDC");
				break;
			case ADC:
				printf("ADC");
				break;
			case SCALER:
				printf("scaler");
				break;
			case PATTERN_UNIT:
				printf("pattern unit");
				break;
			case RPC:
				printf("RPC");
				break;
			default:
				printf("unknown");
		}
		printf(")\n");
		printf("\tmodule_data_format: 0x%x (", vme_subblock_type.module_data_format);
		switch (vme_subblock_type.module_data_format) {
			case MODULE_INFO:
				printf("module info");
				break;
			case EVENT_DATA:
				printf("event data");
				break;
			default:
				printf("unknown");
		}
		printf(")\n");
		printf("\tmodule_identifier: 0x%x\n", vme_subblock_type.module_identifier);
	}
	printf("\tsubblock.subblock_header.subblock_content_length: %" PRIu16 " bytes\n", subblock->subblock_header.subblock_content_length);
	printf("\tsubblock.content:\n");
	uint8_t *byte = subblock->subblock_content;
	for (uint16_t byte_index = 0; byte_index < subblock->subblock_header.subblock_content_length; byte_index += 4) {
		printf("\t\t0x%04x: %02x %02x %02x %02x\n", byte_index, *(byte + byte_index), *(byte + byte_index + 1), *(byte + byte_index + 2), *(byte + byte_index + 3));
	}
}

void print_block(struct Block *block) {
	char block_magic_number[sizeof block->block_header.block_magic_number + 1];
	snprintf(block_magic_number, sizeof block->block_header.block_magic_number + 1, "%s", block->block_header.block_magic_number);
	printf("block.block_header.block_magic_number: %s\n", block_magic_number);
	printf("block.block_header.block_type: 0x%" PRIx8 " (", block->block_header.block_type);
	switch (block->block_header.block_type) {
		case RUN_START:
			printf("run start");
			break;
		case EVENT:
			printf("event");
			break;
		case RUN_STOP:
			printf("run stop");
			break;
		default:
			printf("unknown");
	}
	printf(")\n");
	printf("block.block_header.block_content_length: %" PRIuFAST32 " bytes\n", get_block_content_length(block));
	printf("block.block_header.record_number: %" PRIu32 "\n", block->block_header.record_number);
	struct timespec tp;
	get_block_timestamp(block, &tp);
	printf("block.block_header.timestamp: %jd.%09ld s\n", (intmax_t) tp.tv_sec, tp.tv_nsec);
	printf("block.block_content:\n");
	for (uint32_t subblock_index = 0; subblock_index < block->block_content.n_subblocks; ++subblock_index) {
		print_subblock(block->block_content.subblocks + subblock_index);
	}
}

int malloc_handler(void **ptr, size_t size) {
	*ptr = malloc(size);
	if (ptr == NULL && size != 0) {
		MSG_ERROR_ERRNO("Unable to malloc")
		return 1;
	}
	return 0;
}

int realloc_handler(void **ptr, size_t size) {
	void *temp_ptr = realloc(*ptr, size);
	if (temp_ptr == NULL && size != 0) {
		MSG_ERROR_ERRNO("Unable to realloc")
		return 1;
	}
	*ptr = temp_ptr;
	return 0;
}
