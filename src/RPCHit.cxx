#include "OfflineData/RPCHit.h"

RPCHit::RPCHit(double x, double y, double z, int run_number, int run_time, int strip_number) {
    m_x = x;
    m_y = y;
    m_z = z;
    m_runnumber = run_number;
    m_runtime = run_time;
    m_stripnumber = strip_number;
}

double RPCHit::getX() {
    return m_x;
}

double RPCHit::getY() {
    return m_y;
}

double RPCHit::getZ() {
    return m_z;
}

int RPCHit::getRunNumber() {
    return m_runnumber;
}

int RPCHit::getRunTime() {
    return m_runtime;
}

int RPCHit::getStripNumber() {
    return m_stripnumber;
}
