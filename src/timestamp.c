#include <time.h>
#include <stdio.h>
#include "time_common.h"

static const char STRFTIME_TIME_FORMAT[] = "%F %T";
static const char STRFTIME_TIMEZONE_FORMAT[] = "%Z";

static const size_t MAX_STRFTIME_TIME_OUTPUT_LENGTH = 18 + 1 + 8 + 1;
static const size_t MAX_STRFTIME_TIMEZONE_OUTPUT_LENGTH = 5 + 1;

void print_timestamp(FILE *stream, const struct timespec *tp) {
	struct tm *timeptr;

	char strftime_time_output[MAX_STRFTIME_TIME_OUTPUT_LENGTH];
	char strftime_timezone_output[MAX_STRFTIME_TIMEZONE_OUTPUT_LENGTH];

	timeptr = localtime(&tp->tv_sec);
	strftime(strftime_time_output, MAX_STRFTIME_TIME_OUTPUT_LENGTH, STRFTIME_TIME_FORMAT, timeptr);
	strftime(strftime_timezone_output, MAX_STRFTIME_TIMEZONE_OUTPUT_LENGTH, STRFTIME_TIMEZONE_FORMAT, timeptr);
	fprintf(stream, "%s.%09ld %4s", strftime_time_output, tp->tv_nsec, strftime_timezone_output);
}

void print_current_timestamp(FILE *stream) {
	struct timespec tp;
	clock_gettime(CLOCK_SOURCE, &tp);
	print_timestamp(stream, &tp);
}
