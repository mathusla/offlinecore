#include <cstdio>
#include <cinttypes>
#include <vector>
#include "mathusla_file_format_v3.h"
#include "msgsvc.h"
#include "timestamp.h"
#include "readout.hh"

void clear_amb_event_data_readout(struct AMBEventDataReadout &amb_event_data_readout) {
	amb_event_data_readout.amb_events.clear();
}

enum ambeventsection_t {OUT_OF_EVENT, EVENT_START, AMB_EVENT_NUMBER, TDC_EVENT_NUMBER, TDC_TIME, TDC_ADDRESS, PATTERN_DATA, EVENT_STOP, WORD_COUNT, PARITY};

int fill_amb_event_data_readout(FILE *stream, struct AMBEventDataReadout &amb_event_data_readout) {
	clear_amb_event_data_readout(amb_event_data_readout);
	if (stream == NULL) {
		return 0;
	}
	uint16_t word;
	uint16_t parity = 0;
	unsigned word_count = 0;
	enum ambeventsection_t event_section = OUT_OF_EVENT;
	while (fread(&word, sizeof (uint16_t), 1, stream) == 1) {
		if (event_section == EVENT_STOP) {
			MSG_DEBUG("word count %" PRIu16, word)
			++word_count;
			parity ^= word;
			MSG_DEBUG("parsed word count %u", word_count)
			if (word != word_count) {
				MSG_ERROR("Word count word (%" PRIu16 ") does not match expected word count (%u)", word, word_count)
			}
			event_section = WORD_COUNT;
			continue;
		} else if (event_section == WORD_COUNT) {
			MSG_DEBUG("parity 0x%04" PRIx16, word)
			MSG_DEBUG("parsed parity 0x%04" PRIx16, parity)
			if (word != parity) {
				MSG_ERROR("Parity word (0x%04" PRIx16 ") does not match expected parity (0x%04" PRIx16 ")", word, parity)
			}
			event_section = PARITY;
			continue;
		} else if (event_section == PARITY) {
			event_section = OUT_OF_EVENT;
		}
		MSG_DEBUG("0x%04x", word)
		switch (word) {
			case 0xffff:
				if (event_section != OUT_OF_EVENT) {
					MSG_ERROR("0xffff in event")
				}
				continue;
				break;
			case 0x0aaa:
				if (event_section != OUT_OF_EVENT) {
					MSG_ERROR("Event start in event")
					return 1;
				}
				MSG_DEBUG("event start")
				{
					struct AMBEvent amb_event;
					amb_event_data_readout.amb_events.push_back(amb_event);
					word_count = 1;
					parity = word;
				}
				event_section = EVENT_START;
				continue;
				break;
			case 0x0555:
				if (event_section != AMB_EVENT_NUMBER && event_section != TDC_EVENT_NUMBER && event_section != TDC_ADDRESS && event_section != PATTERN_DATA) {
					MSG_ERROR("Event stop out of place")
					return 1;
				}
				MSG_DEBUG("event stop")
				++word_count;
				parity ^= word;
				event_section = EVENT_STOP;
				continue;
				break;
			default:
				break;
		}
		if (event_section == OUT_OF_EVENT && word == 0x7001) {
			MSG_DEBUG("event number")
			word_count = 2;
			parity = 0x0aaa ^ 0x7001;
			struct AMBEvent amb_event;
			amb_event.amb_event_number = 1;
			amb_event_data_readout.amb_events.push_back(amb_event);
			event_section = AMB_EVENT_NUMBER;
			continue;
		}
		if ((word & 0xf800) == 0x7000) {
			if (event_section != EVENT_START) {
				MSG_ERROR("AMB event number out of place")
				return 1;
			}
			MSG_DEBUG("event number")
			++word_count;
			parity ^= word;
			amb_event_data_readout.amb_events.back().amb_event_number = (word & 0x07ff);
			event_section = AMB_EVENT_NUMBER;
		} else if ((word & 0xe000) == 0x4000) {
			if (event_section != AMB_EVENT_NUMBER && event_section != TDC_ADDRESS) {
				MSG_ERROR("TDC event number out of place")
				return 1;
			}
			MSG_DEBUG("TDC event number")
			++word_count;
			parity ^= word;
			struct AMBTDCEvent amb_tdc_event;
			amb_tdc_event.amb_tdc_number = ((word & 0x1800) >> 11);
			amb_tdc_event.amb_tdc_event_number = (word & 0x07ff);
			amb_event_data_readout.amb_events.back().amb_tdc_events.push_back(amb_tdc_event);
			event_section = TDC_EVENT_NUMBER;
		} else if ((word & 0xe000) == 0x2000) {
			if (event_section != TDC_EVENT_NUMBER && event_section != TDC_ADDRESS) {
				MSG_ERROR("TDC data out of place")
				return 1;
			}
			MSG_DEBUG("TDC data")
			++word_count;
			parity ^= word;
			if ((((unsigned) word & 0x1800) >> 11) != amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_number) {
				MSG_ERROR("TDC number in TDC data word (%" PRIu16 ") doesn't match TDC event number word (%u)", (word & 0x1800) >> 11, amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_event_number)
			}
			struct AMBTDCHit amb_tdc_hit;
			amb_tdc_hit.amb_tdc_time_measurement = (word & 0x07ff);
			amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_hits.push_back(amb_tdc_hit);
			event_section = TDC_TIME;
		} else if ((word & 0xe482) == 0x0000) {
			if (event_section != TDC_TIME) {
				MSG_ERROR("TDC address out of place")
				return 1;
			}
			MSG_DEBUG("TDC adr")
			++word_count;
			parity ^= word;
			if ((((unsigned) word & 0x1800) >> 11) != amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_number) {
				MSG_ERROR("TDC number in TDC address word (%" PRIu16 ") doesn't match TDC event number word (%u)", (word & 0x1800) >> 11, amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_event_number)
			}
			amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_hits.back().amb_tdc_channel_number = ((word & 0x00fc) >> 2);
			amb_event_data_readout.amb_events.back().amb_tdc_events.back().amb_tdc_hits.back().amb_tdc_hit_multiplicity = ((word & 0x0700) >> 8);
			event_section = TDC_ADDRESS;
		} else if ((word & 0x8000) == 0x8000) {
			if (event_section != AMB_EVENT_NUMBER && event_section != TDC_EVENT_NUMBER && event_section != TDC_ADDRESS && event_section != PATTERN_DATA) {
				MSG_ERROR("Strip pattern out of place")
				return 1;
			}
			MSG_DEBUG("strip")
			++word_count;
			parity ^= word;
			struct AMBPadPattern amb_pad_pattern;
			amb_pad_pattern.amb_pad_address = ((word & 0x7f00) >> 8);
			amb_pad_pattern.amb_strip_pattern = (word & 0xff); 
			amb_event_data_readout.amb_events.back().amb_pad_patterns.push_back(amb_pad_pattern);
			event_section = PATTERN_DATA;
		} else if (word == 0) {
			MSG_WARNING("AMB buffer word 0x0000, assuming this is padding")
		} else {
			MSG_ERROR("Unknown AMB buffer word: 0x%" PRIx16, word)
			return 1;
		}
	}
	if ( ! feof(stream)) {
		MSG_ERROR_ERRNO("Unable to read AMB data")
		return 1;
	}
	if (event_section != OUT_OF_EVENT && event_section != PARITY) {
		MSG_ERROR("Incomplete AMB readout")
		return 1;
	}
	return 0;
}

void print_amb_event_data_readout(FILE *stream, const struct AMBEventDataReadout &amb_event_data_readout) {
	for (size_t amb_event_index = 0; amb_event_index < amb_event_data_readout.amb_events.size(); amb_event_index++) {
		fprintf(stream, "\t\tAMB event %ju:\n", amb_event_index);
		fprintf(stream, "\t\t\tAMB event number: %" PRIu16 "\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_event_number);
		for (size_t amb_tdc_event_index = 0; amb_tdc_event_index < amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.size(); amb_tdc_event_index++) {
			fprintf(stream, "\t\t\tAMB TDC event %ju:\n", amb_tdc_event_index);
			fprintf(stream, "\t\t\t\tAMB TDC number: %u\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.at(amb_tdc_event_index).amb_tdc_number);
			fprintf(stream, "\t\t\t\tAMB TDC event number: %u\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.at(amb_tdc_event_index).amb_tdc_event_number);
			for (size_t amb_tdc_hit_index = 0; amb_tdc_hit_index < amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.at(amb_tdc_event_index).amb_tdc_hits.size(); amb_tdc_hit_index++) {
				fprintf(stream, "\t\t\t\tAMB TDC hit %ju:\n", amb_tdc_hit_index);
				fprintf(stream, "\t\t\t\t\tAMB TDC channel number: %u\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.at(amb_tdc_event_index).amb_tdc_hits.at(amb_tdc_hit_index).amb_tdc_channel_number);
				fprintf(stream, "\t\t\t\t\tAMB TDC time measurement: %u\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.at(amb_tdc_event_index).amb_tdc_hits.at(amb_tdc_hit_index).amb_tdc_time_measurement);
				fprintf(stream, "\t\t\t\t\tAMB TDC hit multiplicity: %u\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_tdc_events.at(amb_tdc_event_index).amb_tdc_hits.at(amb_tdc_hit_index).amb_tdc_hit_multiplicity);
			}
		}
		for (size_t amb_pad_pattern_index = 0; amb_pad_pattern_index < amb_event_data_readout.amb_events.at(amb_event_index).amb_pad_patterns.size(); amb_pad_pattern_index++) {
			fprintf(stream, "\t\t\tAMB pad pattern %ju:\n", amb_pad_pattern_index);
			fprintf(stream, "\t\t\t\tAMB pad address: %" PRIu8 "\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_pad_patterns.at(amb_pad_pattern_index).amb_pad_address);
			fprintf(stream, "\t\t\t\tAMB strip pattern: 0x%02" PRIx8 "\n", amb_event_data_readout.amb_events.at(amb_event_index).amb_pad_patterns.at(amb_pad_pattern_index).amb_strip_pattern);
		}
	}
}

void clear_tdc_event_data_readout(struct TDCEventDataReadout &tdc_event_data_readout) {
	tdc_event_data_readout.tdc_events.clear();
}

enum tdcwordtype_t {HEADER_WORD_TYPE = 0x2, DATUM_WORD_TYPE = 0x0, EOB_WORD_TYPE = 0x1, NOT_VALID_DATUM_WORD_TYPE = 0x3};

int fill_tdc_event_data_readout(FILE *stream, struct TDCEventDataReadout &tdc_event_data_readout) {
	clear_tdc_event_data_readout(tdc_event_data_readout);
	if (stream == NULL) {
		return 0;
	}
	uint32_t word;
	int in_event = 0;
	while (fread(&word, sizeof (uint32_t), 1, stream) == 1) {
		switch ((word >> 21) & 0x3) {
			case HEADER_WORD_TYPE:
				if (in_event) {
					MSG_ERROR("TDC header within event")
					return 1;
				}
				{
					struct TDCEvent tdc_event;
					tdc_event.GEO_address = (word >> 27) & 0x1f;
					tdc_event.event_number = word & 0xfff;
					tdc_event_data_readout.tdc_events.push_back(tdc_event);
				}
				in_event = 1;
				break;
			case DATUM_WORD_TYPE:
				if ( ! in_event) {
					MSG_ERROR("TDC datum outside event")
					return 1;
				}
				{
					struct TDCHit tdc_hit;
					tdc_hit.channel_number = (word >> 24) & 0x7f;
					tdc_hit.start = (word >> 23) & 0x1;
					tdc_hit.edge = (word >> 20) & 0x1;
					tdc_hit.time_measurement = word & 0xfffff;
					tdc_event_data_readout.tdc_events.back().tdc_hits.push_back(tdc_hit);
				}
				break;
			case EOB_WORD_TYPE:
				if ( ! in_event) {
					MSG_ERROR("TDC EOB outside event")
					return 1;
				}
				if (tdc_event_data_readout.tdc_events.back().GEO_address != ((word >> 27) & 0x1f)) {
					MSG_ERROR("TDC EOB GEO address does not match header")
					return 1;
				}
				if ((word & 0xffff) != tdc_event_data_readout.tdc_events.back().tdc_hits.size()) {
					MSG_ERROR("TDC EOB event data counter (%" PRIu32 ") does not match number of hits (%ju)", word & 0xffff, tdc_event_data_readout.tdc_events.back().tdc_hits.size())
					return 1;
				}
				tdc_event_data_readout.tdc_events.back().event_status = (word >> 24) & 0x7;
				in_event = 0;
				break;
			case NOT_VALID_DATUM_WORD_TYPE:
				MSG_ERROR("TDC not valid datum")
				break;
			default:
				MSG_ERROR("Unknown TDC word type")
				return 1;
				break;
		}
	}
	if ( ! feof(stream)) {
		MSG_ERROR_ERRNO("Unable to read TDC data")
		return 1;
	}
	if (in_event) {
		MSG_ERROR("Incomplete TDC readout")
		return 1;
	}
	return 0;
}

void print_tdc_event_data_readout(FILE *stream, const struct TDCEventDataReadout &tdc_event_data_readout) {
	for (size_t tdc_event_index = 0; tdc_event_index < tdc_event_data_readout.tdc_events.size(); tdc_event_index++) {
		fprintf(stream, "\t\tTDC event %ju:\n", tdc_event_index);
		fprintf(stream, "\t\t\tTDC GEO address: %u\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).GEO_address);
		fprintf(stream, "\t\t\tTDC event number: %u\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).event_number);
		fprintf(stream, "\t\t\tTDC event status: 0x%03x\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).event_status);
		for (size_t tdc_hit_index = 0; tdc_hit_index < tdc_event_data_readout.tdc_events.at(tdc_event_index).tdc_hits.size(); ++tdc_hit_index) {
			fprintf(stream, "\t\t\tTDC hit %ju:\n", tdc_hit_index);
			fprintf(stream, "\t\t\t\tChannel number: %u\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).tdc_hits.at(tdc_hit_index).channel_number);
			fprintf(stream, "\t\t\t\tStart: %u\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).tdc_hits.at(tdc_hit_index).start);
			fprintf(stream, "\t\t\t\tEdge: %u\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).tdc_hits.at(tdc_hit_index).edge);
			fprintf(stream, "\t\t\t\tTime measurement: %" PRIuFAST32  "\n", tdc_event_data_readout.tdc_events.at(tdc_event_index).tdc_hits.at(tdc_hit_index).time_measurement);
		}
	}
}

void clear_adc_event_data_readout(struct ADCEventDataReadout &adc_event_data_readout) {
	adc_event_data_readout.adc_events.clear();
}

int fill_adc_event_data_readout(FILE *stream, struct ADCEventDataReadout &adc_event_data_readout) {
	clear_adc_event_data_readout(adc_event_data_readout);
	if (stream == NULL) {
		return 0;
	}
	struct ADCEvent adc_event;
	while (fread(&adc_event, sizeof (struct ADCEvent), 1, stream) == 1) {
		adc_event_data_readout.adc_events.push_back(adc_event);
	}
	if ( ! feof(stream)) {
		MSG_ERROR_ERRNO("Unable to read ADC data")
		return 1;
	}
	return 0;
}

void print_adc_event_data_readout(FILE *stream, const struct ADCEventDataReadout &adc_event_data_readout) {
	for (size_t adc_event_index = 0; adc_event_index < adc_event_data_readout.adc_events.size(); adc_event_index++) {
		fprintf(stream, "\t\tADC event %ju:\n", adc_event_index);
		for (size_t adc_channel_index = 0; adc_channel_index < sizeof (struct ADCEvent) / sizeof adc_event_data_readout.adc_events.at(adc_event_index).channel_counts[0]; ++adc_channel_index) {
			if (adc_event_data_readout.adc_events.at(adc_event_index).channel_counts[adc_channel_index] >= (1 << 12)) {
				MSG_ERROR("adc counts too high")
			}
			fprintf(stream, "\t\t\tADC channel %ju counts: %" PRIu16 "\n", adc_channel_index, adc_event_data_readout.adc_events.at(adc_event_index).channel_counts[adc_channel_index]);
		}
	}
}

void print_scaler_event_data_readout(FILE *stream, const struct ScalerEventDataReadout &scaler_event_data_readout) {
	for (size_t scaler_counter_index = 0; scaler_counter_index < sizeof scaler_event_data_readout.counter_values / sizeof scaler_event_data_readout.counter_values[0]; scaler_counter_index++) {
		fprintf(stream, "\t\tCounter %ju value: %" PRIu32 "\n", scaler_counter_index, scaler_event_data_readout.counter_values[scaler_counter_index]);
	}
}

void print_pattern_unit_event_data_readout(FILE *stream, const struct PatternUnitEventDataReadout &pattern_unit_event_data_readout) {
	fprintf(stream, "\t\tPattern register: 0x%02x\n", pattern_unit_event_data_readout.pattern_register);
	fprintf(stream, "\t\tMultiplicity register: 0x%02x\n", pattern_unit_event_data_readout.multiplicity_register);
}

void clear_vme_event_data_readout(struct VMEEventDataReadout &vme_event_data_readout) {
	vme_event_data_readout.tdc_event_data_readouts.clear();
	vme_event_data_readout.adc_event_data_readouts.clear();
	vme_event_data_readout.scaler_event_data_readouts.clear();
	vme_event_data_readout.pattern_unit_event_data_readouts.clear();
	vme_event_data_readout.amb_event_data_readouts.clear();
}


int fill_vme_event_data_readout(struct VMEEventDataReadout &vme_event_data_readout, const struct Block &block) {
	if (block.block_header.block_type != EVENT) {
		MSG_ERROR("Block is not for an event")
		return 1;
	}
	clear_vme_event_data_readout(vme_event_data_readout);
	vme_event_data_readout.event_number = block.block_header.record_number;
	get_block_timestamp(&block, &vme_event_data_readout.timestamp);
	struct Subblock *subblock;
	struct VMESubblockType vme_subblock_type;
	struct TDCEventDataReadout tdc_event_data_readout;
	struct ADCEventDataReadout adc_event_data_readout;
	struct ScalerEventDataReadout scaler_event_data_readout;
	struct PatternUnitEventDataReadout pattern_unit_event_data_readout;
	struct AMBEventDataReadout amb_event_data_readout;
	FILE *stream;
	int status = 0;
	for (uint_fast32_t subblock_index = 0; subblock_index < block.block_content.n_subblocks; subblock_index++) {
		subblock = block.block_content.subblocks + subblock_index;
		get_vme_subblock_type(&vme_subblock_type, subblock->subblock_header.subblock_type);
		stream = fmemopen(subblock->subblock_content, subblock->subblock_header.subblock_content_length, "r");
		if (stream == NULL && subblock->subblock_header.subblock_content_length) {
			MSG_ERROR_ERRNO("Unable to open memory stream")
			continue;
		}
		switch (vme_subblock_type.module_class) {
			case TDC:
				if (fill_tdc_event_data_readout(stream, tdc_event_data_readout) != 0) {
					MSG_ERROR("Cannot read TDC subblock")
					status = 1;
				}
				tdc_event_data_readout.module_identifier = vme_subblock_type.module_identifier;
				vme_event_data_readout.tdc_event_data_readouts.push_back(tdc_event_data_readout);
				break;
			case ADC:
				if (fill_adc_event_data_readout(stream, adc_event_data_readout) != 0) {
					MSG_ERROR("Cannot read ADC subblock")
					status = 1;
				}
				adc_event_data_readout.module_identifier = vme_subblock_type.module_identifier;
				vme_event_data_readout.adc_event_data_readouts.push_back(adc_event_data_readout);
				break;
			case SCALER:
				if (subblock->subblock_header.subblock_content_length != sizeof scaler_event_data_readout.counter_values) {
					MSG_ERROR("Cannot read scaler subblock")
					status = 1;
					break;
				}
				memcpy(scaler_event_data_readout.counter_values, subblock->subblock_content, sizeof scaler_event_data_readout.counter_values);
				scaler_event_data_readout.module_identifier = vme_subblock_type.module_identifier;
				vme_event_data_readout.scaler_event_data_readouts.push_back(scaler_event_data_readout);
				break;
			case PATTERN_UNIT:
				if (subblock->subblock_header.subblock_content_length != sizeof pattern_unit_event_data_readout.pattern_register + sizeof pattern_unit_event_data_readout.multiplicity_register) {
					MSG_ERROR("Cannot read pattern unit subblock")
					status = 1;
					break;
				}
				pattern_unit_event_data_readout.pattern_register = ((uint16_t *) subblock->subblock_content)[0];
				pattern_unit_event_data_readout.multiplicity_register = ((uint16_t *) subblock->subblock_content)[1];
				pattern_unit_event_data_readout.module_identifier = vme_subblock_type.module_identifier;
				vme_event_data_readout.pattern_unit_event_data_readouts.push_back(pattern_unit_event_data_readout);
				break;
			case RPC:
				if (fill_amb_event_data_readout(stream, amb_event_data_readout) != 0) {
					MSG_ERROR("Cannot read AMB subblock")
					status = 1;
				}
				amb_event_data_readout.module_identifier = vme_subblock_type.module_identifier;
				vme_event_data_readout.amb_event_data_readouts.push_back(amb_event_data_readout);
				break;
			default:
				MSG_ERROR("Unknown subblock")
				break;
		}
		if (stream != NULL) {
			fclose(stream);
		}
	}
	return status;
}

void print_vme_event_data_readout(FILE *stream, const struct VMEEventDataReadout &vme_event_data_readout) {
	fprintf(stream, "Event %" PRIu32 ":\n", vme_event_data_readout.event_number);
	fprintf(stream, "Timestamp: ");
	print_timestamp(stream, &vme_event_data_readout.timestamp);
	fputc('\n', stream);
	for (unsigned tdc_event_data_readout_index = 0; tdc_event_data_readout_index < vme_event_data_readout.tdc_event_data_readouts.size(); tdc_event_data_readout_index++) {
		fprintf(stream, "\tModule %u (TDC):\n", vme_event_data_readout.tdc_event_data_readouts.at(tdc_event_data_readout_index).module_identifier);
		print_tdc_event_data_readout(stream, vme_event_data_readout.tdc_event_data_readouts.at(tdc_event_data_readout_index));
	}
	for (unsigned adc_event_data_readout_index = 0; adc_event_data_readout_index < vme_event_data_readout.adc_event_data_readouts.size(); adc_event_data_readout_index++) {
		fprintf(stream, "\tModule %u (ADC):\n", vme_event_data_readout.adc_event_data_readouts.at(adc_event_data_readout_index).module_identifier);
		print_adc_event_data_readout(stream, vme_event_data_readout.adc_event_data_readouts.at(adc_event_data_readout_index));
	}
	for (unsigned scaler_event_data_readout_index = 0; scaler_event_data_readout_index < vme_event_data_readout.scaler_event_data_readouts.size(); scaler_event_data_readout_index++) {
		fprintf(stream, "\tModule %u (scaler):\n", vme_event_data_readout.scaler_event_data_readouts.at(scaler_event_data_readout_index).module_identifier);
		print_scaler_event_data_readout(stream, vme_event_data_readout.scaler_event_data_readouts.at(scaler_event_data_readout_index));
	}
	for (unsigned pattern_unit_event_data_readout_index = 0; pattern_unit_event_data_readout_index < vme_event_data_readout.pattern_unit_event_data_readouts.size(); pattern_unit_event_data_readout_index++) {
		fprintf(stream, "\tModule %u (pattern unit):\n", vme_event_data_readout.pattern_unit_event_data_readouts.at(pattern_unit_event_data_readout_index).module_identifier);
		print_pattern_unit_event_data_readout(stream, vme_event_data_readout.pattern_unit_event_data_readouts.at(pattern_unit_event_data_readout_index));
	}
	for (unsigned amb_event_data_readout_index = 0; amb_event_data_readout_index < vme_event_data_readout.amb_event_data_readouts.size(); amb_event_data_readout_index++) {
		fprintf(stream, "\tModule %u (RPC):\n", vme_event_data_readout.amb_event_data_readouts.at(amb_event_data_readout_index).module_identifier);
		print_amb_event_data_readout(stream, vme_event_data_readout.amb_event_data_readouts.at(amb_event_data_readout_index));
	}
}
