#include <cstdio>
#include <cinttypes>
#include "msgsvc.h"
#include "mathusla_file_format_v3.h"
#include "readout.hh"

const enum msglevel_t OUTPUT_LEVEL = LEVEL_INFO;

int main(int argc, char *argv[]) {
	if (argc != 2) {
		MSG_FATAL("Usage: %s DATAFILE", argv[0])
	}

	FILE *file = fopen(argv[1], "rb");
	if (file == NULL) {
		MSG_FATAL_ERRNO("Unable to open file %s", argv[1],)
	}

	struct FileHeader file_header;
	if (read_file_header(file, &file_header) != 0) {
		fclose(file);
		MSG_FATAL("Unable to read file header")
	}

	struct Block block;
	struct VMESubblockType vme_subblock_type;
	while (get_next_block(file, &block) == 0) {
		if (block.block_header.block_type == RUN_START) {
			MSG_INFO("Run start, run number: %" PRIu32, block.block_header.record_number)
		} else if (block.block_header.block_type == RUN_STOP) {
			MSG_INFO("Run stop, run number: %" PRIu32, block.block_header.record_number)
		} else {
			continue;
		}
		for (uint_fast32_t subblock_index = 0; subblock_index < block.block_content.n_subblocks; subblock_index++) {
			struct Subblock *subblock = block.block_content.subblocks + subblock_index;
			get_vme_subblock_type(&vme_subblock_type, subblock->subblock_header.subblock_type);
			if (vme_subblock_type.module_data_format == MODULE_INFO) {
				struct ModuleInfoController *controller_info;
				struct ModuleInfoTDC *tdc_info;
				struct ModuleInfoADC *adc_info;
				struct ModuleInfoRPC *rpc_info;
				struct ModuleInfoScaler *scaler_info;
				struct ModuleInfoPatternUnit *pattern_unit_info;

				struct ModuleInfoSubblockContentCommon *module_info = (struct ModuleInfoSubblockContentCommon *) subblock->subblock_content;
				MSG_INFO("Module identifier: 0x%x", vme_subblock_type.module_identifier)
				MSG_INFO("Module class: 0x%x", vme_subblock_type.module_class)
				MSG_INFO("Module manufacturer: %.8s", module_info->module_manufacturer)
				MSG_INFO("Module model: %.8s", module_info->module_model)
				MSG_INFO("Module base address: 0x%" PRIx32, module_info->module_base_address)

				switch (vme_subblock_type.module_class) {
					case CONTROLLER:
						controller_info = (struct ModuleInfoController *) subblock->subblock_content + sizeof(struct ModuleInfoSubblockContentCommon);
						break;
					case TDC:
						tdc_info = (struct ModuleInfoTDC *) subblock->subblock_content + sizeof(struct ModuleInfoSubblockContentCommon);
						break;
					case ADC:
						adc_info = (struct ModuleInfoADC *) subblock->subblock_content + sizeof(struct ModuleInfoSubblockContentCommon);
						break;
					case RPC:
						rpc_info = (struct ModuleInfoRPC *) subblock->subblock_content + sizeof(struct ModuleInfoSubblockContentCommon);
						break;
					case SCALER:
						scaler_info = (struct ModuleInfoScaler *) subblock->subblock_content + sizeof(struct ModuleInfoSubblockContentCommon);
						break;
					case PATTERN_UNIT:
						pattern_unit_info = (struct ModuleInfoPatternUnit *) subblock->subblock_content + sizeof(struct ModuleInfoSubblockContentCommon);
						break;
					default:
						break;
				}
			}
		}
		delete_block(&block);
	}
	fclose(file);
	return 0;
}
