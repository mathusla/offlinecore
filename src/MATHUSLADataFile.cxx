#include "MATHUSLADataFile.hh"
#include "msgsvc.h"
#include "mathusla_file_format_v3.h"
#include "readout.hh"

MATHUSLADataFile::MATHUSLADataFile(const char *pathname) {
	m_pathname = pathname;
	m_stream = fopen(m_pathname.c_str(), "rb");
	if (m_stream == NULL) {
		MSG_FATAL_ERRNO("Unable to open file %s", m_pathname.c_str())
	}
	
	struct FileHeader file_header;
	if (read_file_header(m_stream, &file_header) != 0) {
		fclose(m_stream);
		MSG_FATAL("Unable to read file header")
	}
	m_file_format_version = file_header.file_format_version;
}

MATHUSLADataFile::~MATHUSLADataFile() {
	if (m_stream != NULL) {
		if (fclose(m_stream) != 0) {
			MSG_ERROR_ERRNO("Unable to close file %s", m_pathname.c_str())
		}
	}
}

int MATHUSLADataFile::getNextRunStartTime(uint32_t &run_number, struct timespec &ts) {
	struct Block block;
	int return_value;
	while ((return_value = get_next_block(m_stream, &block)) == 0 && block.block_header.block_type != RUN_START) {
		delete_block(&block);
	}
	switch (return_value) {
		case 0:
			break;
		case EOF:
			return EOF;
			break;
		default:
			return 1;
	}
	get_block_timestamp(&block, &ts);
	run_number = block.block_header.record_number;
	delete_block(&block);
	return 0;
}

int MATHUSLADataFile::getNextRunStopTime(uint32_t &run_number, struct timespec &ts) {
	struct Block block;
	int return_value;
	while ((return_value = get_next_block(m_stream, &block)) == 0 && block.block_header.block_type != RUN_STOP) {
		delete_block(&block);
	}
	switch (return_value) {
		case 0:
			break;
		case EOF:
			return EOF;
			break;
		default:
			return 1;
	}
	get_block_timestamp(&block, &ts);
	run_number = block.block_header.record_number;
	delete_block(&block);
	return 0;
}

int MATHUSLADataFile::getNextEvent(struct VMEEventDataReadout &vme_event_data_readout) {
	struct Block block;
	int return_value;
	while ((return_value = get_next_block(m_stream, &block)) == 0 && block.block_header.block_type != EVENT) {}
	switch (return_value) {
		case 0:
			break;
		case EOF:
			return EOF;
			break;
		default:
			return 1;
	}
	if (fill_vme_event_data_readout(vme_event_data_readout, block) != 0) {
		return 1;
	}
	delete_block(&block);
	return 0;
}
