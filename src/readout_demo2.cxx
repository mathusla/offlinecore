#include <cinttypes>
#include "msgsvc.h"
#include "readout.hh"
#include "MATHUSLADataFile.hh"

const enum msglevel_t OUTPUT_LEVEL = LEVEL_INFO;
static const uint64_t UPDATE_INTERVAL = 10000;

int main(int argc, char *argv[]) {
	if (argc != 2) {
		MSG_FATAL("Usage: %s DATAFILE", argv[0])
	}

	MATHUSLADataFile data_file(argv[1]);

	struct VMEEventDataReadout vme_event_data_readout;
	uint64_t n_events = 0;
	while (data_file.getNextEvent(vme_event_data_readout) == 0) {
		n_events++;
		if (n_events % UPDATE_INTERVAL == 0) {
			MSG_INFO("Processed %" PRIu64 " events", n_events)
		}

		// Can put analysis code here using vme_event_data_readout

	}
	MSG_INFO("%" PRIu64 " events", n_events)
	return 0;
}
