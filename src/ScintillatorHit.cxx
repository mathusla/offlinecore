#include "OfflineData/ScintillatorHit.h"

ScintillatorHit::ScintillatorHit(double x, double y, double z, int run_number, int run_time) {
    m_x = x;
    m_y = y;
    m_z = z;
    m_runnumber = run_number;
    m_runtime = run_time;
}

double ScintillatorHit::getX() {
    return m_x;
}

double ScintillatorHit::getY() {
    return m_y;
}

double ScintillatorHit::getZ() {
    return m_z;
}

int ScintillatorHit::getRunNumber() {
    return m_runnumber;
}

int ScintillatorHit::getRunTime() {
    return m_runtime;
}
