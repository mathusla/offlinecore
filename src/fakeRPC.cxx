#include <iostream>
#include "OfflineData/RPCHit.h"

main(){

  RPCHit rpcHit(1, 2, 3, 4, 5, 6);
  std::cout << "X Position: " << rpcHit.getX() << std::endl;
  std::cout << "Y Position: " << rpcHit.getY() << std::endl;
  std::cout << "Z Position: " << rpcHit.getZ() << std::endl;
  std::cout << "Run Number: " << rpcHit.getRunNumber() << std::endl;
  std::cout << "Time: " << rpcHit.getRunTime() << std::endl;
  std::cout << "Strip Number: " << rpcHit.getStripNumber() << std::endl;

}
