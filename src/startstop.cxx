#include <cinttypes>
#include "msgsvc.h"
//#include "readout.hh"
#include "MATHUSLADataFile.hh"
//#include <iostream>

const enum msglevel_t OUTPUT_LEVEL = LEVEL_INFO;
const int AUTO_FLUSH_MSG = 1;
//static const uint64_t UPDATE_INTERVAL = 10000;

int main(int argc, char *argv[]) {
	if (argc != 2) {
		MSG_FATAL("Usage: %s DATAFILE", argv[0])
	}

	MATHUSLADataFile data_file(argv[1]);

	struct timespec ts;
	uint32_t run_number1 = 0;
	uint32_t run_number2 = 0;
	if (data_file.getNextRunStartTime(run_number1, ts) == 0) {
		fprintf(stdout, "%16" PRIu32 "\t", run_number1);
		//print_timestamp(stdout, &ts);
		fprintf(stdout, "%14ju.%09ld", ts.tv_sec, ts.tv_nsec);
		if (data_file.getNextRunStopTime(run_number2, ts) == 0 && run_number1 == run_number2) {
			fputc('\t', stdout);
			//print_timestamp(stdout, &ts);
			fprintf(stdout, "%14ju.%09ld", ts.tv_sec, ts.tv_nsec);
		} else {
		}
	} else {
	}
	fputc('\n', stdout);
	return 0;
}
