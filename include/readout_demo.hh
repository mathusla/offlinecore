#ifndef READOUT_DEMO_HH
#define READOUT_DEMO_HH

#include <cstdint>

const enum msglevel_t OUTPUT_LEVEL = LEVEL_INFO;

static const int PRINT_RAW = 0;
static const int PRINT_READABLE = 1;

static const uint16_t UPDATE_INTERVAL = 10000;

#endif // READOUT_DEMO_HH
