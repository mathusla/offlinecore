/*
	msgsvc.h
	Simplified message service to allow output supression
	Mason Proffitt
	Adapted from Nir Amram
*/

#ifndef MSGSVC_H
#define MSGSVC_H

#include <string.h> // strerror
#include <errno.h> // errno
#include <stdio.h> // printf
#include <stdlib.h> // exit
#include <stdint.h>
#include "timestamp.h"

#define STRERROR strerror(errno)

static const char COLOR_DEFAULT[] = "\033[1;0m";
static const char COLOR_BLACK[] = "\033[1;30m";
static const char COLOR_RED[] = "\033[1;31m";
static const char COLOR_GREEN[] = "\033[1;32m";
static const char COLOR_YELLOW[] = "\033[1;33m";
static const char COLOR_BLUE[] = "\033[1;34m";
static const char COLOR_MAGENTA[] = "\033[1;35m";
static const char COLOR_CYAN[] = "\033[1;36m";
static const char COLOR_WHITE[] = "\033[1;37m";

enum msglevel_t {
	LEVEL_DEBUG_FULL,
	LEVEL_DEBUG,
	LEVEL_INFO,
	LEVEL_WARNING,
	LEVEL_ERROR,
	LEVEL_FATAL
};

extern const enum msglevel_t OUTPUT_LEVEL;
extern const int AUTO_FLUSH_MSG;

#define MSG(level, stream, color, level_prefix, suffix_prefix, suffix, ...)\
	if (level >= OUTPUT_LEVEL) {\
		fputs(color, stream);\
		fputc('[', stream);\
		print_current_timestamp(stream);\
		fprintf(stream, "] %s:%s:%ju: %s", __FILE__, __func__, (uintmax_t)__LINE__, level_prefix);\
		fprintf(stream, __VA_ARGS__);\
		if (suffix[0] != '\0') {\
			fputs(suffix_prefix, stream);\
			fputs(suffix, stream);\
		}\
		fprintf(stream, "%s\n", COLOR_DEFAULT);\
		if (AUTO_FLUSH_MSG != 0) {\
			fflush(stream);\
		}\
	}

#define MSG_DEBUG_FULL(...) MSG(LEVEL_DEBUG_FULL, stderr, COLOR_DEFAULT, "DEBUG INFO: ", "", "", __VA_ARGS__)
#define MSG_DEBUG(...) MSG(LEVEL_DEBUG, stderr, COLOR_CYAN, "DEBUG INFO: ", "", "", __VA_ARGS__)
#define MSG_INFO(...) MSG(LEVEL_INFO, stderr, COLOR_GREEN, "", "", "", __VA_ARGS__)
#define MSG_WARNING(...) MSG(LEVEL_WARNING, stderr, COLOR_YELLOW, "WARNING: ", "", "", __VA_ARGS__)
#define MSG_ERROR(...) MSG(LEVEL_ERROR, stderr, COLOR_RED, "ERROR: ", "", "", __VA_ARGS__)
#define MSG_FATAL(...) {MSG(LEVEL_FATAL, stderr, COLOR_RED, "FATAL ERROR: ", "", "", __VA_ARGS__) exit(1);}

#define MSG_WARNING_ERRNO(...) MSG(LEVEL_WARNING, stderr, COLOR_YELLOW, "WARNING: ", ": ", STRERROR, __VA_ARGS__)
#define MSG_ERROR_ERRNO(...) MSG(LEVEL_ERROR, stderr, COLOR_RED, "ERROR: ", ": ", STRERROR, __VA_ARGS__)
#define MSG_FATAL_ERRNO(...) {MSG(LEVEL_FATAL, stderr, COLOR_RED, "FATAL ERROR: ", ": ", STRERROR, __VA_ARGS__) exit(1);}

#endif // MSGSVC_H
