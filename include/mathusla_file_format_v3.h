#ifndef MATHUSLA_FILE_FORMAT_V3_H
#define MATHUSLA_FILE_FORMAT_V3_H

#include <stdint.h>
#include <stdio.h>
#include <time.h>

static const uint32_t FILE_FORMAT_VERSION = 3;

enum blocktype_t {RUN_START = 0x00, EVENT = 0xaa, RUN_STOP = 0xff};

enum moduleclass_t {CONTROLLER = 0, TDC = 1, ADC = 2, SCALER = 3, PATTERN_UNIT = 4, RPC = 5};
enum moduledataformat_t {MODULE_INFO = 0, EVENT_DATA = 31};

static const uint8_t FILE_MAGIC_NUMBER[] = {'M', 'A', 'T', 'H', 'U', 'S', 'L', 'A'};
static const uint8_t BLOCK_MAGIC_NUMBER[] = {'B', 'L', 'K', 'M'};

static const uint8_t MANUFACTURER_CAEN[] = {'C', 'A', 'E', 'N', 0, 0, 0, 0};
static const uint8_t MANUFACTURER_LECROY[] = {'L', 'e', 'C', 'r', 'o', 'y', 0, 0};
static const uint8_t MANUFACTURER_INFN[] = {'I', 'N', 'F', 'N', 0, 0, 0, 0};

static const uint8_t MODEL_V1718[] = {'V', '1', '7', '1', '8', 0, 0, 0};
static const uint8_t MODEL_V2718[] = {'V', '2', '7', '1', '8', 0, 0, 0};
static const uint8_t MODEL_V767[] = {'V', '7', '6', '7', 0, 0, 0, 0};
static const uint8_t MODEL_1182[] = {'1', '1', '8', '2', 0, 0, 0, 0};
static const uint8_t MODEL_V560N[] = {'V', '5', '6', '0', 'N', 0, 0, 0};
static const uint8_t MODEL_V259N[] = {'V', '2', '5', '9', 'N', 0, 0, 0};
static const uint8_t MODEL_AMB[] = {'A', 'M', 'B', 0, 0, 0, 0, 0};

struct FileHeader {
        uint8_t file_magic_number[sizeof FILE_MAGIC_NUMBER];
        uint32_t file_format_version;
};

struct BlockContentLength {
	uint8_t least_significant_8_bits;
	uint8_t middle_8_bits;
	uint8_t most_significant_8_bits;
};

struct Timestamp {
	uint32_t least_significant_32_bits;
	uint32_t most_significant_32_bits;
};

struct BlockHeader {
	uint8_t block_magic_number[sizeof BLOCK_MAGIC_NUMBER];
	uint8_t block_type;
	struct BlockContentLength block_content_length;
	uint32_t record_number;
	struct Timestamp timestamp;
};

struct SubblockHeader {
	uint16_t subblock_type;
	uint16_t subblock_content_length;
};

struct Subblock {
	struct SubblockHeader subblock_header;
	uint8_t *subblock_content;
};

struct BlockContent {
	struct Subblock *subblocks;
	uint_fast32_t n_subblocks;
};

struct Block {
	struct BlockHeader block_header;
	struct BlockContent block_content;
};

struct VMESubblockType {
	uint_fast8_t module_class;
	uint_fast8_t module_data_format;
	uint_fast8_t module_identifier;
};

struct ModuleInfoSubblockContentCommon {
	uint8_t module_manufacturer[8];
	uint8_t module_model[8];
	uint32_t module_base_address;
};

struct ModuleInfoController {
	uint8_t software_release[8];
	uint8_t driver_release[8];
	uint16_t status_register;
	uint16_t control_register;
	uint16_t firmware_revision_register;
	uint16_t irq_status_register;
	uint16_t input_multiplexer_register;
	uint16_t output_multiplexer_register;
};

struct ModuleInfoTDC {
	uint16_t manufacturers_id[3];
	uint16_t board_id[4];
	uint16_t revision_id;
	uint16_t module_serial_number[2];
	uint16_t geographical_register;
	uint16_t bit_set_register;
	uint16_t interrupt_level_register;
	uint16_t status_register_1;
	uint16_t control_register_1;
	uint16_t address_decoder_register_32;
	uint16_t address_decoder_register_24;
	uint16_t mcst_address_register;
	uint16_t mcst_control_register;
	uint16_t status_register_2;
	uint16_t control_register_2;
	uint16_t event_counter_register;
	uint16_t auto_load;
	uint16_t enable_disable_words[8];
	uint16_t window_width;
	uint16_t window_offset;
	uint16_t trigger_latency;
	uint16_t trigger_configuration;
	uint16_t start_configuration;
	uint16_t channel_adjusts[128];
	uint16_t global_offset;
	uint16_t enable_adjusts;
	uint16_t edge_detection_configuration[3];
	uint16_t almost_full_level;
	uint16_t error_codes_of_tdcs[4];
	uint16_t id_codes_of_tdcs[8];
	uint16_t reject_offset;
	uint16_t look_ahead_window;
	uint16_t look_back_window;
	uint16_t dll_current;
	uint16_t advanced_configuration;
	uint16_t error_mask;
};

struct ModuleInfoADC {
	uint16_t csr0;
	uint16_t zero_padding;
};

struct ModuleInfoScaler {
	uint16_t version_and_series;
	uint16_t manufacturer_and_module_type;
	uint16_t scale_status_register;
	uint16_t request_register;
	uint16_t interrupt_level_and_veto_register;
	uint16_t interrupt_vector_register;
};

struct ModuleInfoPatternUnit {
	uint16_t version_serial_number;
	uint16_t manufacturer_type_number;
};

struct ModuleInfoRPC {
	uint8_t amb_status;
	uint8_t amb_channel_status[4];
	uint8_t zero_padding_loweset_8_bits;
	uint16_t zero_padding_highest_16_bits;
};

int read_file_header(FILE *stream, struct FileHeader *file_header);
void get_vme_subblock_type(struct VMESubblockType *vme_subblock_type, const uint16_t vme_subblock_type_word);
uint16_t get_vme_subblock_type_word(const struct VMESubblockType *vme_subblock_type);
uint_fast32_t get_block_content_length(const struct Block *block);
int set_block_content_length(struct Block *block, const uint_fast32_t length);
void get_block_timestamp(const struct Block *block, struct timespec *tp);
int set_block_timestamp(struct Block *block, const struct timespec *tp);
void initialize_file_header(struct FileHeader *file_header, const uint32_t file_format_version);
void initialize_block(struct Block *block, const uint8_t block_type, const uint32_t record_number);
void pad_subblock(struct Subblock *subblock);
int add_subblock(struct Block *block, const struct Subblock *subblock);
int write_file_header(FILE *stream, const struct FileHeader *file_header);
FILE *create_mathusla_file(const char *filename);
int write_subblock(FILE *stream, const struct Subblock *subblock);
int write_block(FILE *stream, const struct Block *block);
void delete_subblock(struct Subblock *subblock);
void delete_block(struct Block *block);
int get_next_subblock(FILE *stream, struct Subblock *subblock);
int get_next_block(FILE *stream, struct Block *block);
void print_file_header(struct FileHeader *file_header);
void print_subblock(struct Subblock *subblock);
void print_block(struct Block *block);
int malloc_handler(void **ptr, size_t size);
int realloc_handler(void **ptr, size_t size);

#endif //MATHUSLA_FILE_FORMAT_V3_H
