#ifndef TIME_COMMON_H
#define TIME_COMMON_H

#include <sys/types.h>

static const clockid_t CLOCK_SOURCE = CLOCK_REALTIME;

#endif // TIME_COMMON_H
