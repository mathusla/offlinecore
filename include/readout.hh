#ifndef READOUT_HH
#define READOUT_HH

#include <cstdint>
#include <cstdio>
#include <vector>

struct TDCHit {
	unsigned channel_number;
	unsigned start;
	unsigned edge;
	uint_fast32_t time_measurement;
};

struct TDCEvent {
	unsigned GEO_address;
	unsigned event_number;
	unsigned event_status;
	std::vector<struct TDCHit> tdc_hits;
};

struct TDCEventDataReadout {
	unsigned module_identifier;
	std::vector<struct TDCEvent> tdc_events;
};

struct ADCEvent {
	uint16_t channel_counts[8];
};

struct ADCEventDataReadout {
	unsigned module_identifier;
	std::vector<struct ADCEvent> adc_events;
};

struct ScalerEventDataReadout {
	unsigned module_identifier;
	uint32_t counter_values[16];
};

struct PatternUnitEventDataReadout {
	unsigned module_identifier;
	uint16_t pattern_register;
	uint16_t multiplicity_register;
};

struct AMBTDCHit {
	unsigned amb_tdc_channel_number;
	unsigned amb_tdc_time_measurement;
	unsigned amb_tdc_hit_multiplicity;
};

struct AMBTDCEvent {
	unsigned amb_tdc_number;
	unsigned amb_tdc_event_number;
	std::vector<struct AMBTDCHit> amb_tdc_hits;
};

struct AMBPadPattern {
	uint8_t amb_pad_address;
	uint8_t amb_strip_pattern;
};

struct AMBEvent {
	uint16_t amb_event_number;
	std::vector<struct AMBTDCEvent> amb_tdc_events;
	std::vector<struct AMBPadPattern> amb_pad_patterns;
};

struct AMBEventDataReadout {
	unsigned module_identifier;
	std::vector<struct AMBEvent> amb_events;
};

struct VMEEventDataReadout {
	uint32_t event_number;
	struct timespec timestamp;
	std::vector<struct TDCEventDataReadout> tdc_event_data_readouts;
	std::vector<struct ADCEventDataReadout> adc_event_data_readouts;
	std::vector<struct ScalerEventDataReadout> scaler_event_data_readouts;
	std::vector<struct PatternUnitEventDataReadout> pattern_unit_event_data_readouts;
	std::vector<struct AMBEventDataReadout> amb_event_data_readouts;
};

void clear_amb_event_data_readout(struct AMBEventDataReadout &amb_event_data_readout);
int fill_amb_event_data_readout(FILE *stream, struct AMBEventDataReadout &amb_event_data_readout);
void print_amb_event_data_readout(FILE *stream, const struct AMBEventDataReadout &amb_event_data_readout);
void clear_tdc_event_data_readout(struct TDCEventDataReadout &tdc_event_data_readout);
int fill_tdc_event_data_readout(FILE *stream, struct TDCEventDataReadout &tdc_event_data_readout);
void print_tdc_event_data_readout(FILE *stream, const struct TDCEventDataReadout &tdc_event_data_readout);
void clear_adc_event_data_readout(struct ADCEventDataReadout &adc_event_data_readout);
int fill_adc_event_data_readout(FILE *stream, struct ADCEventDataReadout &adc_event_data_readout);
void print_adc_event_data_readout(FILE *stream, const struct ADCEventDataReadout &adc_event_data_readout);
void print_scaler_event_data_readout(FILE *stream, const struct ScalerEventDataReadout &scaler_event_data_readout);
void print_pattern_unit_event_data_readout(FILE *stream, const struct PatternUnitEventDataReadout &pattern_unit_event_data_readout);
void clear_vme_event_data_readout(struct VMEEventDataReadout &vme_event_data_readout);
int fill_vme_event_data_readout(struct VMEEventDataReadout &vme_event_data_readout, const struct Block &block);
void print_vme_event_data_readout(FILE *stream, const struct VMEEventDataReadout &vme_event_data_readout);

#endif //READOUT_HH
