#ifndef MATHUSLADATAFILE_HH
#define MATHUSLADATAFILE_HH

#include <string>
#include <cstdint>
#include <cstdio>

class MATHUSLADataFile {
	public:
		MATHUSLADataFile(const char *pathname);
		~MATHUSLADataFile();
		int getNextEvent(struct VMEEventDataReadout &vme_event_data_readout);
		int getNextRunStartTime(uint32_t &run_number, struct timespec &ts);
		int getNextRunStopTime(uint32_t &run_number, struct timespec &ts);
	private:
		std::string m_pathname;
		uint32_t m_file_format_version;
		FILE *m_stream;
};

#endif // MATHUSLADATAFILE_HH
