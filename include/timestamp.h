#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include <stdio.h>

void print_timestamp(FILE *stream, const struct timespec *tp);
void print_current_timestamp(FILE *stream);

#endif // TIMESTAMP_H
