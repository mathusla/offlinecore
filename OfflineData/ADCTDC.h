class ADCTDC {
    public:
  ATCTDC(ADC &adc, TDC &tdc);

        int getTDCCount();
        int getChannelNumber();
        double getCorrectedTime();
    private:
        ADC *m_adc;
        TDC *m_tdc;
};
