class TDC {
    public:
  TDC(int TDC_count, int channel_number, int start_bit, int edge_bit);

        int getTDCCount();
        int getChannelNumber();
        int getStartBit();
	int getEdgeBit();
    private:
        int m_tdccount;
        int m_channelnumber;
        int m_startbit;
	int m_edgebit;
};
