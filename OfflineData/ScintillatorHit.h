class ScintillatorHit {
    public:
        ScintillatorHit(double x, double y, double z, int run_number, int run_time);
        double getX();
        double getY();
        double getZ();
        int getRunNumber();
        int getRunTime();
    private:
        double m_x, m_y, m_z;
        int m_runnumber;
        int m_runtime;
};