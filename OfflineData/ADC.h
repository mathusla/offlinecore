class ADC {
    public:
  ADC(int ADC_count, int channel_number, int ADC_number);
        
        int getADCCount();
        int getChannelNumber();
	int getADCNumber();
    private:
        int m_adccount;
        int m_channelnumber;
	int m_adcnumber;
};
