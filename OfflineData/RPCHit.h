class RPCHit {
    public:
  RPCHit(double x, double y, double z, int run_number, int run_time , int strip_number);
        double getX();
        double getY();
        double getZ();
        int getRunNumber();
        int getRunTime();
	int getStripNumber();
    private:
        double m_x, m_y, m_z;
        int m_runnumber;
        int m_runtime;
	int m_stripnumber;
};
