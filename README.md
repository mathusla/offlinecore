For test stand run start/stop times (assuming you already have a reasonably recent GCC version set up):

```bash
make
./startstop <raw datafile>
```

For example, if EOS is already mounted, the start and stop times of all good runs can be printed with:

```bash
for file in /eos/project/m/mathusla/teststand/data/run_000000{{3785..3805},{3807..3815},{3817..3866},{3869..3964},{3966..4034},{4036..4101},{4104..4130},{4133..4145},{4147..4205},{4219..4242},{4245..4250},{4257..4304},{4307..4504}}.mathusla
do
	./startstop $file
done
```


The details of the unpacked data structures can be seen in include/readout.hh.

An example of decoding is available with readout_demo. Compiling should be as simple as `make readout_demo`.

The program can be run with:

./readout_demo FILE [EVENT_NUMBER]

where FILE is the pathname of the datafile. If the optional EVENT_NUMBER is specified, only the specified event will be displayed.

The test stand data files are stored and should be readable in /eos/user/m/mathusla/mathusla175/daq/data/runs/.
